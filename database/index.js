const Sequelize = require('sequelize')
const dotenv = require('dotenv')
dotenv.config()

const orm = new Sequelize(process.env.DB_NAME, process.env.DB_USER, '', {
    host: process.env.DB_HOST,
    dialect: 'mysql'
})

// Define Table and Sync Table

let users = orm.define('users', {
    id: {
        type: Sequelize.DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },
    firstName: {
        type: Sequelize.DataTypes.STRING,
        allowNull: false
    },
    lastName: {
        type: Sequelize.DataTypes.STRING
    }
})

module.exports.users = users

// Create Users

let createUser = (fname, lname) => new Promise((res, rej) => {
    users.create({
        firstName: fname,
        lastName: lname
    })
        .then(res)
        .catch(rej)
})

module.exports.createUser = createUser

// Get users

let getUsers = (id) => new Promise((res, rej) => {
    if (id !== undefined) {
        users.findAll({
            where: {
                id: id
            }
        })
            .then(res)
            .catch(rej)
    }
    else {
        users.findAll({ raw: true })
            .then(res)
            .catch(rej)
    }
})



module.exports.getUsers = getUsers

// Update user

let updateUser = (id, fname, lname) => new Promise((res, rej) => {
    users.update({
        firstName: fname,
        lastName: lname
    }, {
        where: {
            id: id
        }
    })
        .then(res)
        .catch(rej)
})

module.exports.updateUser = updateUser

// Delete user

let deleteUser = id => new Promise((res, rej) => {
    users.destroy({
        where: {
            id: id
        }
    })
        .then(res)
        .catch(rej)
})

module.exports.deleteUser = deleteUser
